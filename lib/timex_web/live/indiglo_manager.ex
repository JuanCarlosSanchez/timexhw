defmodule TimexWeb.IndigloManager do
  use GenServer

  def init(ui) do
    :gproc.reg({:p, :l, :ui_event})
    {:ok, %{ui_pid: ui, st: :IndigloOff}}
  end

  def handle_info(:"top-right", %{ui_pid: ui, st: :IndigloOff} = state) do
    GenServer.cast(ui, :set_indiglo)
    {:noreply, %{state | st: IndigloOn}}
  end

  def handle_info(:"top-right", %{ui_pid: ui, st: :IndigloOn} = state) do
    Process.send_after(self(), :hold, 2000)
    GenServer.cast(ui, :set_indiglo)
    {:noreply, %{state | st: Waiting}}
  end

  def handle_info(:hold, %{ui_pid: ui, st: Waiting} = state) do
    GenServer.cast(ui, :unset_indiglo)
    {:noreply, %{state | st: IndigloOff}}
  end

  def handle_info(:start_alarm, %{ui_pid: ui} = state) do
      GenServer.cast(ui, :set_indiglo)
      Process.send_after(self(), :alarm_working_1, 1000)
    {:noreply, %{state | st: IndigloOn}}
  end

  def handle_info(:alarm_working_1, %{ui_pid: ui, st: IndigloOn} = state) do
    GenServer.cast(ui, :unset_indiglo)
    Process.send_after(self(), :alarm_working_2, 1000)
    {:noreply, %{state | st: IndigloOff}}
  end

  def handle_info(:alarm_working_2, %{ui_pid: ui, st: IndigloOff} = state) do
    GenServer.cast(ui, :set_indiglo)
    Process.send_after(self(), :alarm_working_3, 1000)
    {:noreply, %{state | st: IndigloOn}}
  end

  def handle_info(:alarm_working_3, %{ui_pid: ui, st: IndigloOn} = state) do
    GenServer.cast(ui, :unset_indiglo)
    Process.send_after(self(), :alarm_working_4, 1000)
    {:noreply, %{state | st: IndigloOff}}
  end

  def handle_info(:alarm_working_4, %{ui_pid: ui, st: IndigloOff} = state) do
    GenServer.cast(ui, :set_indiglo)
    Process.send_after(self(), :alarm_On_to_Off, 1000)
    {:noreply, %{state | st: IndigloOn}}
  end

  def handle_info(:alarm_On_to_Off, %{ui_pid: ui, st: IndigloOn} = state) do
    GenServer.cast(ui, :unset_indiglo)
    {:noreply, %{state | st: IndigloOff}}
  end

  def handle_info(_event, state), do: {:noreply, state}

end
